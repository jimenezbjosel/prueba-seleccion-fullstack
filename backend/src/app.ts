import express from 'express';

require('express-async-errors');

import cors from 'cors';
import { json } from 'body-parser';
import { NotFoundError } from './errors/not-found-error';
import { errorHandler } from './middlewares/error-handlers';
import { charactersRouter } from './routes/characters';

const app = express();
app.use(cors());

app.use(charactersRouter);
app.use(json());

app.all('*', async (req, res) => {
    throw new NotFoundError();
});

app.use(errorHandler);

export { app };
