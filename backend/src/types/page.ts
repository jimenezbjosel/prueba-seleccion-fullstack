interface Page<T> {
  page: number;
  totalPages: number;
  data: T[];
}
