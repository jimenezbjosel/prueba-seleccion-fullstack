import { ICharacter } from '../types/character';
import https from 'https';
import { InternalError } from '../errors/internal-error';

const CHARACTER_API = process.env.CHARACTER_API || 'https://api.got.show/api/book/characters';

interface ExternalCharacter extends ICharacter {
  _id?: string;
}

export class Character {
  static async download(): Promise<ExternalCharacter[]> {
    return new Promise((resolve, reject) => {
      let body: string = '';

      https.get(CHARACTER_API, res => {
        if (res.statusCode && (res.statusCode < 200 || res.statusCode >= 300)) {
          return reject(new InternalError(`Invalid status code downloading characters ${res.statusCode}`))
        };

        res.on('data', chunk => {
          body += chunk.toString();
        });

        res.on('end', () => {
          try {
            return resolve(JSON.parse(body));
          } catch(e) {
            return reject(e);
          }
        });
      }).on('error', e => {
        return reject(e);
      });
    });
  }
}
