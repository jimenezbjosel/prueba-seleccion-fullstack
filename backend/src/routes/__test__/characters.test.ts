import request from 'supertest';
import { app } from '../../app';

it('endpoint de obtener personajes retorna 200', async () => {
  return request(app)
      .get('/characters')
      .expect(200)
});

it('endpoint retorna un objeto pagina valido', async () => {
  const response = await request(app)
      .get('/characters');

  expect(response.body.data).not.toBeUndefined();
  expect(response.body.page).not.toBeUndefined();
  expect(response.body.totalPages).not.toBeUndefined();
});


it('endpoint retorna una lista no vacia de personajes', async () => {
  const response = await request(app)
      .get('/characters');

  expect(response.body.data).not.toHaveLength(0);
});

it('endpoint retorna un personaje valido', async () => {
  const response = await request(app)
      .get('/characters');

  expect(response.body.data).not.toHaveLength(0);

  const character = response.body.data[0];

  expect(character.name).not.toBeUndefined();
  expect(character.name).toEqual("Abelar Hightower");
});
