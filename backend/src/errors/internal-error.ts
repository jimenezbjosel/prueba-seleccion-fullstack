import { CustomError } from "./custom-error";

export class InternalError extends CustomError {
    statusCode = 500;
    reason: string;

    constructor(reason: string) {
        super(reason);
        this.reason = reason;
        Object.setPrototypeOf(this, InternalError.prototype);
    }

    serializeErrors() {
        return [
            { message: this.reason }
        ]
    }
}

