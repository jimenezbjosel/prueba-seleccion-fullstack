import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { CharacterModel } from '../models/character';

let mongo: any;
beforeAll(async () => {
  process.env.JWT_KEY = 'uyquesecretoestetest'
  mongo = new MongoMemoryServer();

  const mongoUri = await mongo.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
});


beforeEach(async () => {
  const collections = await mongoose.connection.db.collections();

  for (let collection of collections) {
    await collection.deleteMany({});
  }

  CharacterModel.insertMany([{
    "titles": [
        "Ser"
    ],
    "spouse": [],
    "children": [],
    "allegiance": [
        "House Hightower"
    ],
    "books": [
        "The Hedge Knight"
    ],
    "externalId": "5cc08e61888dfb00103cd5e9",
    "name": "Abelar Hightower",
    "slug": "Abelar_Hightower",
    "gender": "male",
    "culture": "Reach",
    "house": "House Hightower",
    "pagerank": {
        "title": "Abelar_Hightower",
        "rank": 7
    },
  }])
});

afterAll(async () => {
  await mongoose.connection.close();
  await mongo.stop();
})
