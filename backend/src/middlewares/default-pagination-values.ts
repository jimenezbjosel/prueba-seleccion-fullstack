import { Request, Response, NextFunction } from 'express';

export const defaultPaginationValues = (req: Request, res: Response, next: NextFunction) => {
    if (!req.query.limit) {
      req.query.limit = '10';
    }

    if (!req.query.page) {
      req.query.page = '1';
    }

    return next();
}
