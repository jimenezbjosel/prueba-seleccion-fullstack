import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card, ListGroup } from 'react-bootstrap';
import Axios from 'axios';

const CharacterData = () => {
  const { id } = useParams();

  const [character, setCharacter] = useState();

  useEffect(() => {
    const getData = async () => {
      const character = (await Axios.get(`http://localhost:3001/characters/${id}`)).data;
      setCharacter(character);
    }

    getData();
  }, []);

  return (
    <>
    { character &&
      <Card>
        <Card.Header></Card.Header>
        <Card.Body>
          <Card.Title>{character.name}</Card.Title>
          <Card.Img variant="top" src={character.image} style={{
              "width": "20%",
              "height": "12vw"
            }}
          />
        </Card.Body>

          <ListGroup variant="flush">
            <ListGroup.Item>{`Libros: ${character.books.join(', ')}`}</ListGroup.Item>
            <ListGroup.Item>{`Titulos: ${character.titles.join(', ')}`}</ListGroup.Item>
            <ListGroup.Item>{`Casa: ${character.house}`}</ListGroup.Item>
            <ListGroup.Item>{`Genero: ${character.gender}`}</ListGroup.Item>
            <ListGroup.Item>{`Slug: ${character.slug}`}</ListGroup.Item>
            <ListGroup.Item>{`Slug: ${character.slug}`}</ListGroup.Item>
            <ListGroup.Item>{`Rank Title: ${character.pagerank.title}`}</ListGroup.Item>
            <ListGroup.Item>{`Page Rank: ${character.pagerank.rank}`}</ListGroup.Item>
          </ListGroup>
      </Card>
    }
    </>);
}

export default CharacterData;
