import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Table, Container, Row, Col, Form , Button, Pagination } from 'react-bootstrap';
import get from 'lodash/get';
import { Link } from 'react-router-dom';

const keys = [
  // "titles",
  // "books",
  "name",
  // "slug",
  // "gender",
  // "image",
  "house",
  // "pagerank.title",
  // "pagerank.rank",
]

const DEFAULT_LIMIT = 10;

const CharacterTable = () => {
  const [page, setPage] = useState(1);
  const [pageData, setPageData] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [searchText, setSearchText] = useState('');

  const searchData = async () => {
    const { data } = await Axios.get(`http://localhost:3001/characters?page=${page}&limit=${DEFAULT_LIMIT}&text=${searchText}`);
    setTotalPages(data.totalPages);
    setPageData(data.data);
  }

  useEffect(() => {
    searchData();
  }, [page]);

  return (
    <Container>
      <Row className="my-1">
        <Col>
          <Form>
            <Form.Group controlId="formSearch">
              <Form.Label>Buscar por nombre o casa</Form.Label>
              <Form.Control
                onChange={e => {
                  e.preventDefault();
                  setSearchText(e.target.value);
                }}
                type="text"
                placeholder="Introduce texto"
              />
            </Form.Group>
            <Button
              onClick={() => {
                setPage(1);
                searchData();
              }}
              variant="primary"
              disabled={searchText.length !== 0 && searchText.length < 3}
            >
              Buscar
            </Button>
            </Form>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table striped bordered hover>
            <thead>
              <tr>
                {keys.map(key => <th>{key}</th>)}
              </tr>
            </thead>
            <tbody>
              {pageData.map(character => (
                <tr>
                  {keys.map(key => <th>{get(character, key, '')}</th>)}
                  <th><Link to={`/character/${character.id}`}>Más información</Link></th>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
      <Row>
        <Col>
          <Pagination>
            <Pagination.First onClick={() => setPage(1)} disabled={totalPages === 0}/>
            <Pagination.Prev onClick={() => setPage(page - 1)} disabled={page <= 1} />
            <Pagination.Ellipsis onClick={() => setPage(page - DEFAULT_LIMIT)} disabled={page - DEFAULT_LIMIT < 1} />

            {page - 2 >= 1 && <Pagination.Item onClick={() => setPage(page - 2)} disabled={page - 2 < 1}>{page - 2}</Pagination.Item> }
            {page - 1 >= 1 && <Pagination.Item onClick={() => setPage(page - 1)} disabled={page - 1 < 1}>{page - 1}</Pagination.Item> }
            <Pagination.Item active>{page}</Pagination.Item>
            <Pagination.Item onClick={() => setPage(page + 1)} disabled={page + 1 > totalPages}>{page + 1}</Pagination.Item>
            <Pagination.Item onClick={() => setPage(page + 2)} disabled={page + 1 > totalPages}>{page + 2}</Pagination.Item>

            <Pagination.Ellipsis onClick={() => setPage(page + DEFAULT_LIMIT)} disabled={page + DEFAULT_LIMIT > totalPages}/>
            <Pagination.Next onClick={() => setPage(page + 1)} disabled={page + 1 > totalPages}/>
            <Pagination.Last onClick={() => setPage(totalPages)} disabled={totalPages === 0}/>
          </Pagination>
        </Col>
      </Row>
    </Container>
  );
}

export default CharacterTable;
