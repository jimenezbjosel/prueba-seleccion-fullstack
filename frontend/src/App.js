import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import CharacterData from './components/CharacterData';
import CharacterTable from './components/CharacterTable';

function App() {
  return (
    <Container>
      <Row>
        <Col>
          <Router>
            <Switch>
              <Route path="/character/:id">
                <CharacterData />
              </Route>
              <Route path="/">
                <CharacterTable />
              </Route>
            </Switch>
          </Router>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
